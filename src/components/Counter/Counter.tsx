// /src/components/Counter/Counter.tsx
'use client';
import {useState} from "react";

export default function Counter() {
    const [count, setCount] = useState(0);
    console.log('Counter component..');
    return (
        <>
            <p>{count}</p>
            <button onClick={() => setCount(count + 1)}>숫자 증가</button>
        </>
    )
}