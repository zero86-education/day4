import Image from 'next/image'
import styles from './page.module.css'
import Counter from "@/components/Counter/Counter";

export default function Home() {

  console.log('HOME - server');

  return (
    <main className={styles.main}>
      <div className={styles.center}>
        <Image
          className={styles.logo}
          src="/next.svg"
          alt="Next.js Logo"
          width={180}
          height={37}
          priority
        />
      </div>
      <Counter />
    </main>
  )
}
