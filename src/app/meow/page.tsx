// /src/app/meow/page.tsx


// 단위는 sec
// epxort const revalidate = false; // false | 'force-cache' | 0 | number
// export const revalidate = 3600 // revalidate at most every hour
export default async function MeowPage() {
    const res = await fetch('https://meowfacts.herokuapp.com', {
        next: { revalidate: 0 }, // SSR
        // cache: 'no-store'
    });
    const data = await res.json();
    const factText = data.data[0];
    return (
        <>
            <h1>meow page!</h1>
            <article>{factText}</article>
        </>
    );
}
