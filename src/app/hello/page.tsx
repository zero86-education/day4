// /src/app/hello/page.tsx

import path from "path";
import {promises as fs} from "fs";

export const revalidate = 3; // 3초마다, 페이지를 재생성
export default async function HelloPage(props: any) {
    const names = await getNames();

    return (
        <div>
            hello!
            {names.map((obj: {name: string; price: number}) => <div key={obj.name}>
                <p>{obj.name}</p>
                <p>{obj.price}</p>
            </div>)}
        </div>
    )
}
const getNames = async () => {
    const filePath = path.join(process.cwd(), 'src', 'json', 'test.json');
    const data = await fs.readFile(filePath, 'utf-8');
    return JSON.parse(data);
}