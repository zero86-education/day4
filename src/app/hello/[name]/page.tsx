// /app/hello/[name]/page.tsx

type Props = {
    params: {
        name: string;
    };
};

export function generateMetadata({params}: Props) {
    return {
        title: `나의 이름: ${params.name}`,
    };
}

export default function HelloPage({params: {name}}: Props) {
    return (
        <div>
            <p>hello page</p>
            <p>{name}</p>
        </div>
    )
}

export async function generateStaticParams() {
    // 모든 제품의 페이지들을 미리 만들어 둘 수 있게 해줄거임 (SSG)
    const names = ['test', 'test2', 'test3', 'test4'];
    return names.map(name => ({ name }));
}